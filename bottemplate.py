from datetime import datetime
from disco.bot import Plugin
from disco.bot import parser
from disco.types.message import MessageEmbed
from db import *
from disco.types.base import Unset
import disco.util.snowflake as snowflake

class BotTemplate(Plugin):
	def __init__(self, bot, config):
		Plugin.__init__(self, bot=bot, config=config)
		if not self.client.config.configured_db:
			setupDb(self.client.config.db)

	@Plugin.command('ping')
	def ping(self, event):
		event.msg.reply("pong")
	
        
        # util methods

	def getExistingOrEmpty(self, collection, id, guild_id):
		if collection.exists(id=id, guild_id=guild_id):
			return collection.get(id=id, guild_id=guild_id)
		else:
			return collection(id=id, guild_id=guild_id)
			
	def getObjectOrNone(self, collection, id, guild_id):
		if collection.exists(id=id, guild_id=guild_id):
			return collection.get(id=id, guild_id=guild_id)
		else:
			return None
	
	def getSetting(self, collection, guild_id):
		if collection.exists(guild_id=guild_id):
			return collection.get(guild_id=guild_id)
		else:
			return None
			
	
	def checkAuthor(self, id):
		return id in self.client.config.admins		
	
	def addToCollection(self, collection, id, event):
		if not self.checkAuthor(event.author.id):
			return
		self.getExistingOrEmpty(collection, id, event.guild.id)
		commit()
	
	def removeFromCollection(self, collection, id, event):
		if not self.checkAuthor(event.author.id):
			return
		
		removeRow = self.getObjectOrNone(collection, id, event.guild.id)
		if (removeRow != None):
			removeRow.delete()
			commit()
			return True
		return False
	

