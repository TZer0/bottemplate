from pony.orm import *

db = Database()

def setupDb(name):
	set_sql_debug(False)
	db.bind(provider='sqlite', filename=name, create_db=True)
	db.generate_mapping(create_tables=True)
	commit()
