Setting up:
Copy config.json.example to config.json and change the values.

Install deps:
```
pip3 install gevent==1.4.0 disco-py==0.0.13rc2 pony
```
To run:
```
python -m disco.cli --config config.json
```
